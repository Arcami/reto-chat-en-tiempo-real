package co.com.sofka.runners.channel;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/realtimeChatUser.feature"},
        glue = {"co.com.sofka.stepdefinition.channel"},
        tags = "@DeleteChannel"
)

public class DeleteChannelTest {
}
