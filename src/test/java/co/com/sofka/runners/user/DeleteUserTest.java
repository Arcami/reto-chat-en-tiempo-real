package co.com.sofka.runners.user;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/realtimeChatUser.feature"},
        glue = {"co.com.sofka.stepdefinition.user"},
        tags = "@DeleteUser"
)

public class DeleteUserTest {
}
