package co.com.sofka.setup;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.log4j.PropertyConfigurator;

import static co.com.sofka.util.Log4JValues.LOG4J_PROPERTIES_FILE_PATH;

public class Setup {

    private static final String BASE_URI = "https://realtime-chat-app-sofkau.herokuapp.com/";
    protected Response response;
    protected RequestSpecification request;

    protected void generalSetup(){
        setUpLog4j2();
        setUpBases();
    }

    private void setUpLog4j2(){
        PropertyConfigurator.configure(LOG4J_PROPERTIES_FILE_PATH.getValue());
    }

    private void setUpBases(){
        RestAssured.baseURI = BASE_URI;
    }
}
