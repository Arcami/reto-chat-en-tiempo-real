package co.com.sofka.stepdefinition.channel;

import co.com.sofka.model.channel.ChannelModel;
import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import java.io.File;

import static co.com.sofka.util.Dictionary.CHANNEL_RESOURCE;
import static co.com.sofka.util.Dictionary.NEW_CHANNEL_NAME;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class UpdateChannelStepDefinition extends Setup {
    private final File file = new File(System.getProperty("user.dir") + "/src/test/resources/files/channelToUpdate.json");
    private static final Logger LOGGER = Logger.getLogger(UpdateChannelStepDefinition.class);

    @Given("that the user entered the channel page")
    public void thatTheUserEnteredTheChannelPage() {
        try {
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON)
                    .body(file);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }
    @When("the user request to update a channel")
    public void theUserRequestToUpdateAChannel() {
        try{
            response = request.when()
                    .log()
                    .all()
                    .put(CHANNEL_RESOURCE);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }

    }
    @Then("the system will update that channel info")
    public void theSystemWillUpdateThatChannelInfo() {
        ChannelModel channelModel =
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_OK)
                .extract().as(ChannelModel.class);

        assertThat(channelModel.getName(), equalTo(NEW_CHANNEL_NAME));
    }
}
