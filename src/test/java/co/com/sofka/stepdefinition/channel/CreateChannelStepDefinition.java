package co.com.sofka.stepdefinition.channel;

import co.com.sofka.model.channel.ChannelModel;
import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import java.io.File;

import static co.com.sofka.util.Dictionary.CHANNEL_NAME;
import static co.com.sofka.util.Dictionary.CHANNEL_RESOURCE;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CreateChannelStepDefinition extends Setup {
    private final File file = new File(System.getProperty("user.dir") + "/src/test/resources/files/newChannel.json");
    private static final Logger LOGGER = Logger.getLogger(CreateChannelStepDefinition.class);


    @Given("that the user entered the channels page")
    public void thatTheUserEnteredTheChannelsPage() {
        try{
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON)
                    .body(file);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }
    @When("the user does the process to create a new channel")
    public void theUserDoesTheProcessToCreateANewChannel() {
        try{
            response = request.when()
                    .log()
                    .all()
                    .post(CHANNEL_RESOURCE);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("the system will create a new channel")
    public void theSystemWillCreateANewChannel() {
        ChannelModel channelModel =
                response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().as(ChannelModel.class);

        assertThat(channelModel.getId(), is(notNullValue()));
        assertThat(channelModel.getName(), equalTo(CHANNEL_NAME));
    }
}
