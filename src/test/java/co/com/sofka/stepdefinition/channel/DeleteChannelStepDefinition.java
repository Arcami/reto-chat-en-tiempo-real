package co.com.sofka.stepdefinition.channel;

import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static co.com.sofka.util.Dictionary.DELETE_CHANNEL_RESOURCE;
import static io.restassured.RestAssured.given;

public class DeleteChannelStepDefinition extends Setup {
    private static final Logger LOGGER = Logger.getLogger(DeleteChannelStepDefinition.class);
    @Given("That the user entered the channel page")
    public void thatTheUserEnteredTheChannelPage() {
        try {
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }
    @When("the user request to delete a specific channel")
    public void theUserRequestToDeleteASpecificChannel() {
        try{
            response = request.when()
                    .log()
                    .all()
                    .delete(DELETE_CHANNEL_RESOURCE);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }

    }
    @Then("the system will delete the selected channel")
    public void theSystemWillDeleteTheSelectedChannel() {
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }


}
