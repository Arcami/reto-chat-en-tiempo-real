package co.com.sofka.stepdefinition.channel;

import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static co.com.sofka.util.Dictionary.CHANNEL_RESOURCE;
import static io.restassured.RestAssured.given;
public class GetChannelListStepDefinition extends Setup {
    private static final Logger LOGGER = Logger.getLogger(GetChannelListStepDefinition.class);


    @Given("that the user browsed to the channel page")
    public void thatTheUserBrowsedToTheChannelPage() {
        try {
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }

    @When("the user request to see the channel list")
    public void theUserRequestToSeeTheChannelList() {
        try{
                response = request.when()
                        .log()
                        .all()
                        .get(CHANNEL_RESOURCE);

        } catch (Exception e){
                LOGGER.error(e.getMessage(),e);
                Assertions.fail(e.getMessage());
        }
    }
    @Then("the system will show the channel list")
    public void theSystemWillShowTheChannelList() {
                response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_OK);
    }
}
