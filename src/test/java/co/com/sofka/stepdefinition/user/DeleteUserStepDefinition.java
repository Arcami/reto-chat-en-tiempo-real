package co.com.sofka.stepdefinition.user;

import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static co.com.sofka.util.Dictionary.DELETE_USER_RESOURCE;
import static io.restassured.RestAssured.given;

public class DeleteUserStepDefinition extends Setup {
    private static final Logger LOGGER = Logger.getLogger(DeleteUserStepDefinition.class);

    @Given("that the user browsed to the contacts page")
    public void thatTheUserBrowsedToTheContactsPage() {
        try{
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }
    @When("the user request to delete a specific contact")
    public void theUserRequestToDeleteASpecificContact() {
        try{
            response = request.when()
                    .log()
                    .all()
                    .delete(DELETE_USER_RESOURCE);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("the system will delete that specific contact")
    public void theSystemWillDeleteThatSpecificContact() {
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }
}
