package co.com.sofka.stepdefinition.user;

import co.com.sofka.model.user.UserModel;
import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import java.io.File;

import static co.com.sofka.util.Dictionary.USER_EMAIL;
import static co.com.sofka.util.Dictionary.USER_RESOURCE;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class CreateUserStepDefinition extends Setup {

    private final File file = new File(System.getProperty("user.dir") + "/src/test/resources/files/newUser.json");
    private static final Logger LOGGER = Logger.getLogger(CreateUserStepDefinition.class);

    @Given("that the user entered in the register page")
    public void thatTheUserEnteredInTheRegisterPage() {
        try{
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON)
                    .body(file);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }

    }

    @When("the user fills the register fields")
    public void theUserFillsTheRegisterFields() {
        try{
            response = request.when()
                    .log()
                    .all()
                    .post(USER_RESOURCE);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }

    }

    @Then("the system will create an user")
    public void theSystemWillCreateAnUser() {
        UserModel userModel = response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().as(UserModel.class);

        assertThat(userModel.getUserName(), is(USER_EMAIL));
        assertThat(userModel.getId(), is(notNullValue()));

    }
}
