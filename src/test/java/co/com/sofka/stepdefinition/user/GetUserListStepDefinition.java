package co.com.sofka.stepdefinition.user;

import co.com.sofka.setup.Setup;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static co.com.sofka.util.Dictionary.USER_RESOURCE;
import static io.restassured.RestAssured.given;

public class GetUserListStepDefinition extends Setup {
    private static final Logger LOGGER = Logger.getLogger(GetUserListStepDefinition.class);

    @Given("that the user entered the contacts page")
    public void thatTheUserEnteredTheContactsPage() {
        try{
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }
    @When("the user request to see all the contacts")
    public void theUserRequestToSeeAllTheContacts() {
        try{
            response = request.when()
                    .log()
                    .all()
                    .get(USER_RESOURCE);

        } catch (Exception e){
            LOGGER.error(e.getMessage(),e);
            Assertions.fail(e.getMessage());
        }
    }
    @Then("the system will show the user list")
    public void theSystemWillShowTheUserList() {
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_OK);
    }

}
