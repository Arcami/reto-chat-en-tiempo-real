Feature: Real time chat
    As an user
    I need to have a real time chat web page
    To be able to use the talk with my partners

    @CreateUser
    Scenario: Create user
        Given that the user entered in the register page
        When the user fills the register fields
        Then the system will create an user

    @ShowUserList
        Scenario: Show user list
        Given that the user entered the contacts page
        When the user request to see all the contacts
        Then the system will show the user list

    @DeleteUser
        Scenario: Delete user
        Given that the user browsed to the contacts page
        When the user request to delete a specific contact
        Then the system will delete that specific contact

    @CreateChannel
    Scenario: Create channel
        Given that the user entered the channels page
        When the user does the process to create a new channel
        Then the system will create a new channel


    @ShowChannelList
    Scenario: Show channel list
        Given that the user browsed to the channel page
        When the user request to see the channel list
        Then the system will show the channel list

    @DeleteChannel
    Scenario: Delete channel
        Given That the user entered the channel page
        When the user request to delete a specific channel
        Then the system will delete the selected channel

    @UpdateChannel
    Scenario: Update channel
        Given that the user entered the channel page
        When the user request to update a channel
        Then the system will update that channel info
