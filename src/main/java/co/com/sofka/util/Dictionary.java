package co.com.sofka.util;

public class Dictionary {

    public static final String USER_EMAIL = "carlos1234567@gmail.com";
    public static final String CHANNEL_NAME = "Juan's test";
    public static final String USER_RESOURCE = "/user";
    public static final String CHANNEL_RESOURCE = "/channel";
    public static final String DELETE_CHANNEL_RESOURCE = "/channel/62bbe8bc407d773941cae515";
    public static final String NEW_CHANNEL_NAME = "Juan's test test";

    public static final String DELETE_USER_RESOURCE = "/user/deleteUser/62bbe5d0407d773941cae514";



    private Dictionary(){}

}
